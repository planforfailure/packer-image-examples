To alleviate issues with any hard coding credentials or access keys in `ex8.json`, we have  exported these variables locally as you would normally within [Linux](https://bash.cyberciti.biz/guide/Export_Variables)

By setting it this also removes any variables printed to standard out or log file. 
```json

   "sensitive-variables": ["access_key", "secret_key"]
```
