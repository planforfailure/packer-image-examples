https://www.packer.io/plugins/builders/amazon#authentication

The AWS provider offers a flexible means of providing credentials for authentication. The following methods are supported, in this order, and explained below:

- Static credentials
- Environment variables
- Shared credentials file
- EC2 Role

- https://www.packer.io/plugins/builders/amazon#environment-variables
- https://www.packer.io/plugins/builders/amazon#shared-credentials-file
- https://www.packer.io/plugins/builders/amazon#iam-task-or-instance-role
