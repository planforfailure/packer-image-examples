### Building Custom Packer Images
An excellent  walkthrough on installing and building images in Packer from https://www.youtube.com/c/SanjeevThiyagarajan. All the examples are done in AWS in the main and each lesson builds on the prior one from [lesson 5](https://www.youtube.com/watch?v=vOV74gevFgs&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=5) or [project_1](project_1) onwards. The playlist can be found [here](https://www.youtube.com/playlist?list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj)

##### Communicators
https://www.packer.io/docs/communicators  _(ssh, winrm)_

##### Builders
https://www.packer.io/docs/builders  _(cloud, ansible, chef)_

##### Provisioners
https://www.packer.io/docs/provisioners  _(file, shell)_

##### Post-Processors
https://www.packer.io/docs/post-processors   _(manifest, artifacts, vagrant)_

 - [Automation with Hashicorp Packer #1: Intro mutable vs Immutable infrastructure](https://www.youtube.com/watch?v=tbv1lTF1wFU&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=1)
 - [Automation with Hashicorp Packer #2: Windows Installation](https://www.youtube.com/watch?v=M8nB46A9iVs&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=2)
 - [Automation with Hashicorp Packer #3: Mac Installation](https://www.youtube.com/watch?v=vOlqThdX4C0&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=3)
 - [Automation with Hashicorp Packer #4: Linux Installation](https://www.youtube.com/watch?v=RSoir5KYNgY&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=4)
 - [Automation with Hashicorp Packer #5: Building Your First AMI](https://www.youtube.com/watch?v=vOV74gevFgs&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=5)
 - [Automation with Hashicorp Packer #6: Configuring Provisioners](https://www.youtube.com/watch?v=TmMoTseT2Ow&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=6)
 - [Automation with Hashicorp Packer #7: Script Provisioner](https://www.youtube.com/watch?v=cORDcA4j02k&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=7)
 - [Automation with Hashicorp Packer #8: File Provisioner](https://www.youtube.com/watch?v=2PduIgZDW9Y&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=8)
 - [Automation with Hashicorp Packer #9: Azure Image & multiple providers](https://www.youtube.com/watch?v=JzFS8l0xNRQ&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=9)
 - [Automation with Hashicorp Packer #10: Post Processors](https://www.youtube.com/watch?v=JqWDqDcO7gw&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=10)
 - [Automation with Hashicorp Packer #11: User Variables](https://www.youtube.com/watch?v=XuyKg85lMYM&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=11)
 - [Automation with Hashicorp Packer #12: Environment Variables](https://www.youtube.com/watch?v=1nJtPWbC8YE&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=12)
 - [Automation with Hashicorp Packer #13: AWS Authentication](https://www.youtube.com/watch?v=hozvC3wsB4w&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=13)


##### DigitalOcean
- https://slugs.do-api.dev/
- https://www.youtube.com/watch?v=_Wk3jMKLQ1I&t=461s
- https://github.com/MasonEgger-Demos/tt-packer-ansible-droplet-image
- https://www.digitalocean.com/community/books/how-to-manage-remote-servers-with-ansible-ebook
- https://docs.digitalocean.com/reference/doctl/
- https://docs.digitalocean.com/products/images/custom-images/

##### Tutorials
- https://developer.hashicorp.com/packer/tutorials/docker-get-started/docker-get-started-build-image
- https://developer.hashicorp.com/packer/tutorials/aws-get-started/aws-get-started-build-image
- https://developer.hashicorp.com/packer/docs/templates/hcl_templates/blocks/build/provisioner #run on specific sources
---
- https://developer.hashicorp.com/packer/tutorials/aws-get-started/aws-get-started-variables  **N.B** 
The order of ascending precedence is: variable defaults, environment variables, variable file(s), command-line flag, this (command-line) flag has the highest precedence and will override values defined by other methods.

**Packer** will automatically load any variable file that matches the name `*.auto.pkrvars.hcl`, without the need to pass the file via the command line, so you can run:-

`packer build .`
