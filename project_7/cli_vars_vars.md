To parse variables at the command line instead of your packer json file, first remove the hard coded ones defined in `ex7.json` to show

```json
{
    "variables": {
     "description": "",
     "version": ""
   },
```

 then run the following:
 
 `$ packer build  -var  'description=nginxCLvarsbuild' -var "version=2.4.0" ex7.json`

 **N.B.** Single quotes on a Linux host, as above. Its double quotes if on MS.


 However once your defined variables gets beyond this number, its easier to define them in their own `variables.json` as below, ensuring you remove this block from `ex7.json`

 ```json
{
    "variables": {
     "description": "",
     "version": ""
   },
```

The `variables.json` should look like this

```json
{
    "variables": {
     "description": "customvarfile",
     "version": "2.4.0.1"
   }
```

`$ packer build -var-file=variables.json ex7.json`



